#include "ShiningEnginePCH.h"
#include "RenderComponent.h"
#include "ResourceManager.h"
#include "Texture2D.h"
#include "Renderer.h"

RenderComponent::RenderComponent(const std::string& textureName)
	:Component{}
	, m_pTexture{ Shining::ResourceManager::GetInstance().LoadTexture(textureName)}
{
}

void RenderComponent::Render(const glm::vec3& pos) const
{
	Shining::Renderer::GetInstance().RenderTexture(*m_pTexture, pos.x, pos.y);
}

void RenderComponent::Update(const float deltaTime)
{
	//unused function
	//if I exclude these lines the compiler throws a warning
	float temp{ deltaTime };
	temp++;
}