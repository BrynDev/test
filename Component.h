#pragma once
#include "Transform.h"

namespace Shining
{
	//types need to be declared here, otherwise they are not recognized as namespace members
	class Texture2D;
	class Font;
	class GameObject;

	class Component
	{
	public:
		Component() = default;
		virtual ~Component() = default;

		virtual void Update(const float deltaTime) = 0;
		virtual void Render(const glm::vec3& pos) const = 0;

		Component(const Component& other) = delete;
		Component(Component&& other) = delete;
		Component& operator=(const Component& other) = delete;
		Component& operator=(Component&& other) = delete;
	private:
	};
}

