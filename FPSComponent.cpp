#include "ShiningEnginePCH.h"
#include "FPSComponent.h"
#include <SDL.h>
#include "GameObject.h"
#include "TextComponent.h"


FPSComponent::FPSComponent(const Shining::GameObject* pOwner)
	:m_pOwner{pOwner}
{
}

void FPSComponent::Update(const float deltaTime)
{
	unsigned int FPS{ unsigned int((1 / deltaTime)) };
	std::string newFPSText{ std::to_string(FPS) + " FPS" };
	m_pOwner->GetComponent<TextComponent>()->SetText(newFPSText);
}


void FPSComponent::Render(const glm::vec3& pos) const
{
	//unused function
	//if I exclude these lines the compiler throws a warning
	float x{ pos.x };
	++x;
}