#pragma once
#include "Component.h"

class Texture2D;

class RenderComponent final : public Shining::Component
{
public:
	RenderComponent(const std::string& textureName);
	virtual ~RenderComponent() = default;

	virtual void Render(const glm::vec3& pos) const override;
	virtual void Update(const float deltaTime) override;

	RenderComponent(const RenderComponent& other) = delete;
	RenderComponent(RenderComponent&& other) = delete;
	RenderComponent& operator=(const RenderComponent& other) = delete;
	RenderComponent& operator=(RenderComponent&& other) = delete;
private:
	Shining::Texture2D* m_pTexture;
};

