#include "ShiningEnginePCH.h"
#include "ShiningEngine.h"
#include <chrono>
#include <thread>
#include "InputManager.h"
#include "SceneManager.h"
#include "Renderer.h"
#include "ResourceManager.h"
#include <SDL.h>
#include "GameObject.h"
#include "Scene.h"
#include "RenderComponent.h"
#include "TextComponent.h"
#include "FPSComponent.h"
#include <vector>

#include <iostream> //for test

using namespace std;
using namespace std::chrono;

/*GameObject* Shining::ShiningEngine::GetNewGameObject() const
{
	return new Gameobject();
}*/

void Shining::ShiningEngine::Initialize()
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0) 
	{
		throw std::runtime_error(std::string("SDL_Init Error: ") + SDL_GetError());
	}

	m_Window = SDL_CreateWindow(
		"Programming 4 assignment",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		640,
		480,
		SDL_WINDOW_OPENGL
	);
	if (m_Window == nullptr) 
	{
		throw std::runtime_error(std::string("SDL_CreateWindow Error: ") + SDL_GetError());
	}

	Renderer::GetInstance().Init(m_Window);
}

/**
 * Code constructing the scene world starts here
 */
void Shining::ShiningEngine::LoadGame() const
{
	Scene& scene = SceneManager::GetInstance().CreateScene("Demo");

	Shining::GameObject* pBackground{ new Shining::GameObject()};
	pBackground->AddComponent(new RenderComponent("background.jpg"));
	scene.Add(pBackground);

	Shining::GameObject* pLogo{ new Shining::GameObject() };
	pLogo->AddComponent(new RenderComponent("logo.png"));
	pLogo->SetPosition(216, 180);
	scene.Add(pLogo);

	Shining::GameObject* pText{ new Shining::GameObject() };
	pText->AddComponent(new TextComponent("Programming 4 Assignment", "Lingua.otf", SDL_Color{255, 255, 255}, 36));
	pText->SetPosition(80, 20);
	scene.Add(pText);

	Shining::GameObject* pCounter{ new Shining::GameObject() };
	pCounter->AddComponent(new TextComponent("00 FPS", "Lingua.otf", SDL_Color{ 255, 255, 50 }, 17));
	pCounter->AddComponent(new FPSComponent(pCounter));
	pCounter->SetPosition(15, 15);
	scene.Add(pCounter);
}

void Shining::ShiningEngine::Cleanup()
{
	Renderer::GetInstance().Destroy();
	SDL_DestroyWindow(m_Window);
	m_Window = nullptr;
	SDL_Quit();
	SceneManager::GetInstance().Destroy();
	ResourceManager::GetInstance().Destroy();
}

void Shining::ShiningEngine::Run()
{
	Initialize();

	// tell the resource manager where he can find the game data
	ResourceManager::GetInstance().Init("../Data/");

	LoadGame();

	//Game loop based on variable time step (deltaTime gets passed to Update()).
	//This means that the program can accomodate if it's running too slow or too fast
	//by scaling certain values according to deltaTime.
	//This also means that there would be issues in a network multiplayer game,
	//but since Bubble Bobble does not have online multiplayer I think this is fine.

	{
		Renderer& renderer = Renderer::GetInstance();
		SceneManager& sceneManager = SceneManager::GetInstance();
		InputManager& input = InputManager::GetInstance();
		auto prevTime{ high_resolution_clock::now() };
		bool doContinue = true;
		while (doContinue) //game loop
		{
			const auto currentTime = high_resolution_clock::now();
			const float deltaTime{ std::chrono::duration<float>(currentTime - prevTime).count() };
			prevTime = currentTime;

			doContinue = input.ProcessInput(); //detect quit
			sceneManager.Update(deltaTime); //update game objects
			renderer.Render(); //render game objects
		}
	}

	Cleanup();
}