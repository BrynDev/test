#pragma once
struct SDL_Window;
#include <vector>
namespace Shining
{
	class ShiningEngine
	{
	public:
		void Initialize();
		void LoadGame() const;
		void Cleanup();
		void Run();

		//GameObject* GetNewGameObject() const;
	private:
		static const int MsPerFrame = 16; //16 for 60 fps, 33 for 30 fps
		SDL_Window* m_Window{};
	};
}